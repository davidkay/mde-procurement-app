// Plugin for vue-clip file uploader
import Vue from 'vue'
import VueClip from 'vue-clip'

Vue.use(VueClip)
