// import Vue base
import Vue from 'vue'
import Vuetify from 'vuetify'
import colors from 'vuetify/es5/util/colors'

// import CSS
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import '@mdi/font/css/materialdesignicons.css'
import '@fortawesome/fontawesome-free/css/all.css'

Vue.use(Vuetify, {
  iconfont: 'fa',
  theme: {
    primary: colors.lightBlue.darken4,
    secondary: colors.blueGrey.darken3,
    accent: colors.deepPurple.lighten1,
    info: colors.cyan.lighten1,
    warning: colors.amber.lighten2,
    error: colors.red.base,
    success: colors.green.lighten1
  }
})
